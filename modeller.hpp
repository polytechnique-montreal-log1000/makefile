#include "frame.hpp"
#include "tool.hpp"

class Modeller{
    public:
        void printInfo();
        Modeller();
        ~Modeller();
        Tool* tool;
        Frame* frame;

};