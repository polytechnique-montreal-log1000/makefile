#include "modeller.hpp"

Modeller::Modeller(){
    this->frame = new Frame();
    this->tool = new Tool();
}

Modeller::~Modeller(){
    delete this->frame;
    delete this->tool;
}
void Modeller::printInfo(){
    this->frame->printSize();
    this->tool->printName();
}