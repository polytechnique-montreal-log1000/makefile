#include "frame.hpp"
#include "util.h"

Frame::Frame(){
    this->size = 10;
}

void Frame::printSize(){
    std::string message = "The frame size is: " + std::to_string(this->size);
    printMessage(message);
}