logiciel: main.o modeller.o tool.o frame.o
	g++ -o $@ $^

main.o: main.cpp modeller.hpp
	g++ -o $@ -c $<

modeller.o: modeller.cpp modeller.hpp tool.hpp frame.hpp
	g++ -o $@ -c $<

tool.o: tool.cpp tool.hpp util.h
	g++ -o $@ -c $<

frame.o: frame.cpp frame.hpp util.h
	g++ -o $@ -c $< 

clean:
	rm -fv *.o

clean_all: clean
	rm -fv logiciel
	
install_move: logiciel
	install -D logiciel executable/logiciel
	rm -f *.o logiciel

# Exemple d'utilisation de $?. 
#Permet d'afficher tous les fichiers .hpp ou .cpp ayant ete modifiee depuls la derniere fois qu'on a lance la commande
print_recent: $(shell find ./ -type f \( -iname \*.hpp -o -iname \*.cpp \) )
	@touch print_recent
	@echo $?
